#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

void swap(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

int main()
{
	const int arraySize = 10;
	int posElement = 0;
	int array[arraySize]; 
	int steps = 0;
	int sortingChoice = 0;
	int searchingChoice = 0;
	int valueSearch = 0;
	int searchStepTracker = 0;
	bool valueFound = 0;
	bool sortingIsAscending = 0;

	srand(time(NULL)); 

	// assigning random values to the array ( 1 - 69 )
	for (int i = 0; i < arraySize; i++)
		array[i] = rand() % 69 + 1;
	
	// just printing the unsorted array for reference
	cout << "Unsorted array: ";
	for (int j = 0; j < arraySize; j++)
		cout << array[j] << " ";
	cout << endl;
	
	// ask user if ASCENDING OR DESCENDING
	cout << "Array sorting: [1] ASCENDING   [2] DESCENDING" << endl;
	cin >> sortingChoice;
	cout << endl;

	// BUBBLE SORTING the array
	if (sortingChoice == 1)
	{
		sortingIsAscending = 1;
		// ASCENDING
		for (int a = 0; a < arraySize; a++)
			for (int b = 0; b < arraySize - a - 1; b++)
				if (array[b] > array[b + 1])
					swap(array[b], array[b + 1]);
	}

	else if (sortingChoice == 2)
	{
		// DESCENDING
		for (int a = 0; a < arraySize; a++)
			for (int b = 0; b < arraySize - a - 1; b++)
				if (array[b] < array[b + 1]) // this is where ascending and descending differs (either < or > )
					swap(array[b], array[b + 1]);
	}

	else
	{
		cout << "Error, array will remain unsorted" << endl;
	}

	// Print array again
	cout << "Sorted array: ";
	for (int j = 0; j < arraySize; j++)
		cout << array[j] << " ";
	cout << endl;
	
	// LINEAR or BINARY search
	cout << "Searching Method: [1] LINEAR   [2] BINARY" << endl;
	cin >> searchingChoice;
	cout << endl;

	if (searchingChoice == 1)
	{
		// Ask the user for value and LINEAR SEARCH
		cout << "Please input a value for LINEAR SEARCHING: ";
		cin >> valueSearch;
		cout << endl;

		// Linear Search
		for (int k = 0; k < arraySize; k++)
		{
			searchStepTracker++;
			if (array[k] == valueSearch)
			{
				valueFound = 1;
				posElement = searchStepTracker - 1;
				break;
			}
		}
	}

	else if (searchingChoice == 2)
	{
		// Binary Search TRY
		int posMedian = ((arraySize / 2) - 1); //position of the median on the array
		int median = array[posMedian]; //is a number
		int tracker; //tracker or pointer

		// Ask the user for value
		cout << "Please input a value for BINARY SEARCHING: ";
		cin >> valueSearch;
		cout << endl;

		searchStepTracker = 1; //first step is to compare with the value of the median
		posElement = posMedian; // start at the median's position till it's found. 
		if (valueSearch == median)
		{
			valueFound = 1;
		}

		if (sortingIsAscending == 1) // cuz it's different depending on how dataset was sorted
		{
			// FOR ASCENDING
			if (valueFound == 0) // ascend or descend the list
			{
				if (valueSearch > median) // ascend from the median
				{
					tracker = posMedian + 1;

					for (int a = 0; a < (posMedian + 1); a++)
					{
						searchStepTracker++;
						posElement++; // cuz ascending from median
						if (array[tracker] == valueSearch)
						{
							valueFound = 1;
							break;
						}
						tracker++;
					}
				}

				else if (valueSearch < median) //descend from the median
				{
					tracker = posMedian - 1;
					for (int b = 0; b < (posMedian + 1); b++)
					{
						searchStepTracker++;
						posElement--; // cuz descending from median 
						if (array[tracker] == valueSearch)
						{
							valueFound = 1;
							break;
						}
						tracker--;
					}
				}
			}
		}
		
		else //FOR DESCENDING SORT
		{
			if (valueFound == 0) // ascend or descend the list
			{
				if (valueSearch > median) // ascend from the median
				{
					tracker = posMedian - 1;

					for (int a = 0; a < (posMedian + 1); a++)
					{
						searchStepTracker++;
						posElement--; // cuz ascending from median (BUT descending based on sort)
						if (array[tracker] == valueSearch)
						{
							valueFound = 1;
							break;
						}
						tracker++;
					}
				}

				else if (valueSearch < median) //descend from the median
				{
					tracker = posMedian + 1;
					for (int b = 0; b < (posMedian + 1); b++)
					{
						searchStepTracker++;
						posElement++; // cuz descending from median (BUT ASCENDING based on THE SORT)
						if (array[tracker] == valueSearch)
						{
							valueFound = 1;
							break;
						}
						tracker--;
					}
				}
			}
		}
	}

	else
	{
		cout << "No searching method selected" << endl;
	}

	// Output value if found and say how many steps it took (keep a tracker for each step)
	if (valueFound == 1)
		cout << "The element was found in the array at position " << posElement <<  endl;

	else
		cout << "The element is not present in the array." << endl;

	cout << "Number of steps(comparisons) taken: " << searchStepTracker << endl; //

	return 0;
}