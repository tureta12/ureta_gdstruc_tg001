#include <iostream>

using namespace std;

int computeSumOfDigits(int number)
{
	if (number == 0)
		return 0;

	int currentDigit = number % 10;
	cout << currentDigit << " ";
	return currentDigit + (computeSumOfDigits(number / 10));
}

int printFibonacci(int n)
{
	if ((n == 0) || (n == 1))
		return n;
	else
		return printFibonacci(n - 1) + printFibonacci(n - 2);
}

bool checkIfPrimeOrNot(int n, int m = 2)
{
	// m starts at 2 because by default a number is already divisible by itself
	if (n <= 2)
		return (n == 2) ? true : false;
	if (n % m == 0)
	{
		cout << "Number is divisble by divisor: " << m << endl;
		return false;
	}
	// The next step is necessary so that we won't have to iterate through all the numbers less than the given number
	// A number cannot be divisible by a divisor whose twice is larger than the number.
	if (m * m > n)
		return true;

	return checkIfPrimeOrNot(n, m + 1);
}

int main()
{
	// #1 Compute the sum of digits of a number
	cout << "SUM OF DIGITS OF A NUMBER" << endl;
	int sumDigitNumber = 0;
	cout << "Input: ";
	cin >> sumDigitNumber;
	cout << "Adding..." << endl;
	int sumOfDigits = computeSumOfDigits(sumDigitNumber);
	cout << "\nThe sum of the digits is: " << sumOfDigits << endl;
	system("pause");

	// #2 Print the fibonacci numbers up to n
	cout << "\nPRINTING FIBONACCI UP TO INPUT" << endl;
	int n, i = 0;
	cout << "Input: ";
	cin >> n;
	while (true)
	{
		int currentValue = printFibonacci(i);
		if (currentValue <= n)
		{
			cout << " " << currentValue;
			i++;
		}
		else
			break;
	}
	cout << endl;
	system("pause");

	// #3 Check a number whether it is prime or not
	cout << "\nCHECK IF PRIME OR NOT" << endl;
	int number = 0;
	cout << "Input: ";
	cin >> number;
	if (checkIfPrimeOrNot(number))
		cout << "The number is PRIME" << endl;
	else
		cout << "The number is NOT PRIME" << endl;

	system("pause");
	return 0;
}