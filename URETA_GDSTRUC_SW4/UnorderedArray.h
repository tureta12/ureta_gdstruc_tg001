#pragma once
#include <assert.h>

// We be making our own vector

// Assertions are mainly used to check logically impossible situations. 
// For example, they can be used to check the state a code expects before it starts running or 
// state after it finishes running. Unlike normal error handling, assertions are generally 
// disabled at run-time.
// Expression to be evaluated. If this expression evaluates to 0, this causes an assertion failure that terminates the program.

template<class T>  // T is just a placeholder. It is a type that is being passed (eg int, string, anything)

				   // Usually we put definitions in  the cpp but due to time constraints and for the purposes of the discussion it's just in the h
class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0) // Take note of growBy value. We can do it like this so we won't have to input growBy
	{
		// What we did here is that we initialized them in the constructor. It is the same as putting the below. Sir Dale said this is faster
		// mArray = NULL 
		// mMaxSize = 0

		if (size)
		{
			mMaxSize = size;

			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize); // memset allocates memory and sets a value per element. sizeof gets the number of bytes 

			mGrowSize = ((growBy > 0) ? growBy : 0); // This is a ternary operator. (( question) ? if yes : if not); 
		}
	}

	virtual ~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value) // For putting stuff at the b ack
	{
		assert(mArray != NULL); // We feed a condition to assert. If true, then the next block of code is executed

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value; // Put the value in the last index 
		mNumElements++;
	}

	virtual T& operator[](int index) // For printing out
	{
		// It does the Grades[0] printing out thing. Operator[] refers to the []. T& takes the memory address of the current. 
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize() // Returns the size 
	{
		return mNumElements;
	}

	virtual void pop() // "Deletes" the last element. It still has something in it, but it isn't counted in our vector. The thought process here is that when you add  to the vector you just replace this. 
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index) // Removes at that index

	{
		assert(mArray != NULL);

		if (index >= mMaxSize) // Index is more than the current size. We are trying to access an element that doesn't exist 
			return;

		// We need to shift everything after the index 1 postiion backwards
		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual int linearSearch(int value)
	{
		for (int i = 0; i < mMaxSize; i++)
		{
			if (value == mArray[i])
			{
				return i;
				break;
			}
		}
		return -1; // If not found. There must be a cleaner and more sensible way to do this though
	}

private:
	T* mArray; // this is the Hungarian Notation where we put m in front of variable name
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T* temp = new T[mMaxSize + mGrowSize]; // Make a temp array 

		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize); // stands for mem copy

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};
