#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "UnorderedArray.h"

using namespace std;

int main()
{
	int unorderedArraySize = 0;
	int randomNumber = 0;
	int indexToDelete = 0;
	int valueToSearch = 0;
	int indexOfSearchValue = 0;
	srand(time(NULL));

	//  Create an UnorderedArray of integers based on the size that the user wants
	cout << "Please enter the size of the UnorderedArray: ";
	cin >> unorderedArraySize;

	UnorderedArray<int> integers(unorderedArraySize);

	// Fill the UnorderedArray with random values from 1 to 100. Output Everything
	cout << "Filling in values..." << endl;
	for (int i = 0; i < unorderedArraySize; i++)
	{
		randomNumber = rand() % 100 + 1;
		integers.push(randomNumber);
	}

	for (int i = 0; i < integers.getSize(); i++)
		cout << integers[i] << " ";

	// Ask the user to remove an element of his/her choice. Output everything. 
	cout << "\n\nPlease enter the index of an element to delete: ";
	cin >> indexToDelete;
	cout << integers[indexToDelete] << " has successfully been removed!" << endl;
	integers.remove(indexToDelete);

	for (int i = 0; i < integers.getSize(); i++)
		cout << integers[i] << " ";

	// Ask the user for a value to search for inside the array. Output the index if the value was found, otherwise print "Value not found."
	cout << "\n\nPlease enter a value to search for inside the array: ";
	cin >> valueToSearch;
	indexOfSearchValue = integers.linearSearch(valueToSearch);
	if (indexOfSearchValue != -1)
		cout << "Value found! The index of " << valueToSearch << " is " << indexOfSearchValue << endl;
	else
		cout << "Value not found." << endl;

	system("pause");
}