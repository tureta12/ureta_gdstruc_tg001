#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include <unordered_set>
#include "OrderedArray.h"

using namespace std;

void reverseString(string word)
{
	if (word.size() == 0) return;
	reverseString(word.substr(1));
	cout << word[0];
}

void bubbleSort(int array[], int arraySize)
{
	for (int i = 0; i < arraySize - 1; i++)
		for (int j = 0; j < arraySize - i - 1; j++)
			if (array[j] > array[j + 1])
			{
				int temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
}

void recursiveGetTriplets(int array[], int& i, int targetSum, int sizeOfArray, unordered_set<string>& uniqueTriple, vector<int> triple, vector<vector<int>>& listOfTriples)
{
	if (i == sizeOfArray - 2)
		return;
	int j = i + 1;
	int k = j + 1;
	for (int j = 0; j < sizeOfArray - 1; j++)
	{
		for(int k = 0; k < sizeOfArray; k++)
		{
			if (array[i] + array[j] + array[k] == targetSum)
			{
				// Check for unique triple
				// Convert into string and check with the unordered set if there's a duplicate 
				string temp = to_string(array[i]) + " : " + to_string(array[j]) + " : " + to_string(array[k]);
				if (uniqueTriple.find(temp) == uniqueTriple.end())
				{
					uniqueTriple.insert(temp);
					triple.push_back(array[i]);
					triple.push_back(array[j]);
					triple.push_back(array[k]);
					listOfTriples.push_back(triple);
					// Clear the triple vector
					triple.clear();
				}
			}
		}
	}
	i += 1;
	return recursiveGetTriplets(array, i, targetSum, sizeOfArray, uniqueTriple, triple, listOfTriples);
}


// I added this block of code to OrderedArray.h
/*virtual void recursiveBinarySearch(T target, int counter, int left, int right)
{
	if (left > right)
	{
		cout << target << " not found! Algorithm took " << counter < , " steps." << endl;
		return;
	}
	counter++;
	int mid = left + (right - left) / 2;
	// If element in the middle
	if (mArray[mid] == target)
	{
		cout << target << " found! Algorithm took " << counter << " steps." << endl;
		return;
	}
	// If element is less than mid
	else if (target < mArray[mid])
	{
		return recursiveBinarySearch(target, counter, left, mid - 1);
	}
	// If element is greater than mid
	else
		return recursiveBinarySearch(target, counter, mid + 1, right);
} */

int main()
{
	srand(time(NULL));

	// #1 Reverse String 
	string stringToReverse;
	cout << "Enter a string to reverse: ";
	getline(cin, stringToReverse);
	cout << "Reversed: ";
	reverseString(stringToReverse);
	cout << endl;
	system("pause");

	// #2 Find Triplets (3 numbers that add up to 6)
	int givenSum = 0;
	int count = 0;
	vector<int> temporaryTriple;
	vector<vector<int>> listOfTriples;
	std::unordered_set<string> uniqueTriples;
	const int arraySize = 10;

	// Generate random numbers and display them in an unordered array for display purposes 
	int randomUnorderedArray[arraySize];
	for (int i = 0; i < arraySize; i++)
		randomUnorderedArray[i] = rand() % 8 + 1;

	cout << "\nRandom Array: ";
	for (int i = 0; i < arraySize; i++)
		cout << randomUnorderedArray[i] << " ";
	cout << "\nPlease enter a given sum: ";
	cin >> givenSum;
	bubbleSort(randomUnorderedArray, arraySize);
	recursiveGetTriplets(randomUnorderedArray, count, givenSum, arraySize, uniqueTriples, temporaryTriple, listOfTriples);

	// Print out the triples if there are any in the vector
	if (!listOfTriples.empty())
	{
		cout << "Triplets found: " << endl;
		for (int i = 0; i < listOfTriples.size(); i++)
		{
			for (int j = 0; j < listOfTriples[i].size(); j++)
				cout << listOfTriples[i][j] << " ";
			cout << endl;
		}
	}
	else
		cout << "No triplets were found!" << endl;
	system("pause");

	// #3 Perform binary search on sorted array
	int numberToSearch;
	int counter = 0;
	OrderedArray<int> sortedArray(arraySize);
	for (int i = 0; i < arraySize; i++)
	{
		int randomNumber = rand() % 99 + 1;
		sortedArray.push(randomNumber);
	}

	// Print out for reference
	cout << "\nRandom Array: ";
	for (int i = 0; i < arraySize; i++)
		cout << sortedArray[i] << " ";

	cout << "\nPlease enter a number to look for: ";
	cin >> numberToSearch;

	sortedArray.recursiveBinarySearch(numberToSearch, counter, 0, arraySize - 1);

	system("pause");
	return 0;
}