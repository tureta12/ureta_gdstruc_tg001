#pragma once
#include "UnorderedArray.h"

template<class T>
class Stack
{
public:
	Stack(int size)
	{
		UnorderedArray<T>* anArray = new UnorderedArray<T>(size);
		mContainer = anArray;
	}

	virtual ~Stack()
	{
		mContainer->~UnorderedArray();
		delete mContainer;
	}

	virtual void push(T value)
	{
		mContainer->pushFront(value);
	}

	virtual void pop() 
	{
		mContainer->popFront();
	}

	virtual void remove(int index)
	{
		mContainer->remove(index);
	}

	virtual void print()
	{
		mContainer->print();
	}

	virtual void empty()
	{
		mContainer->empty();
	}

	virtual const T& top() 
	{
		return mContainer->getTop();
	}

	virtual int getSize()
	{
		return mContainer->getSize();
	}

	virtual const T& operator[](int index)
	{
		return mContainer->operator[](index);
	}

private:
	UnorderedArray<T>* mContainer;
};