#include "Queue.h"
#include "Stack.h"

int main()
{
	int size;
	int choice;
	int elementToPush;
	bool isRunning = true;
	cout << "Enter size for element sets: ";
	cin >> size;
	Stack<int> stack(size);
	Queue<int> queue(size);

	while (isRunning)
	{
		cout << "What do you want to do?" << endl;
		cout << "1 - Push elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything then empty set" << endl;
		cout << "4 - Exit" << endl;

		cin >> choice;
		switch (choice)
		{
		case 1: cout << "Enter a number to push: ";
			cin >> elementToPush;
			cout << endl;
			queue.push(elementToPush);
			stack.push(elementToPush);
			break;

		case 2: cout << "Front / Top elements popped" << endl;
			queue.pop();
			stack.pop();
			break;

		case 3: cout << "Queue elements: " << endl;
			queue.print();
			cout << "\nStack elements: " << endl;
			stack.print();
			cout << "\nEmptying sets..." << endl;
			queue.empty();
			stack.empty();
			break;

		case 4: cout << "Exiting program..." << endl;
			isRunning = false;
			break;

		default: cout << "Invalid input." << endl;
			cin.clear();
			cin.ignore();
		}

		if (choice != 4)
		{
			cout << "Top element of sets:" << endl;
			cout << "Queue: ";
			if (queue.top() != NULL)
				cout << queue.top();
			cout << endl;

			cout << "Stack: ";
			if (stack.top() != NULL)
				cout << stack.top();
			cout << endl;
		}
		system("pause");
		system("cls");
	}
	return 0;
}