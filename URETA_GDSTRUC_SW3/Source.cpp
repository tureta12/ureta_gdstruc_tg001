#include <iostream>
#include <string>

using namespace std;

void printMembers(string guildName, int guildSize, string guildMembers[])
{
	// Print every member
	cout << "\n" << guildName << " members:" << endl;
	for (int j = 0; j < guildSize; j++)
	{
		cout << "Member" << j + 1 << ": " << guildMembers[j] << endl;
	}
}
void getValidInput(int & input)
{
	// Loop till input is valid
	while (true)
	{
		cin >> input;
		if (cin.fail())
		{
			cout << "\nInvalid input. Please try again." << endl;
			cin.clear();
			cin.ignore();
		}

		else
		{
			break;
		}
	}
}

// Function that is badly written. Is it possible to make a function that handles resizing for both adding and deleting members?
// void resizeArray(string previousArray[], int guildSize)
//{
//	// Transfer the contents of the array to a temporary one
//	string* tempArray = new string[guildSize - 1];
//	for (int i = 0; i < guildSize - 1; i++)
//	{
//		tempArray[i] = previousArray[i];
//	}
//
//	// Delete old array
//	delete[] previousArray;
//
//	// Make new array
//	previousArray = new string[guildSize];
//
//	// Transfer contents from temp to new array
//	for (int j = 0; j < guildSize; j++)
//	{
//		previousArray[j] = tempArray[j];
//	}
//
//	// Delete the temp array
//	delete[] tempArray;
//}

int main()
{
	//Create an application form dashboard for a GUILD that your users will use in your game :

	// Declarations and/or initializations
	string guildName;
	string userName;
	string memberToRename;
	string newUsername;
	string deleteUsername;
	string* tempArray;
	int indexGuildMemberRename = 0;
	int indexGuildMemberDelete = 0;
	int guildSize = 0;
	int operationChoice = 0;
	bool isProgramRunning = true;
	bool isMemberToRenameFound = true;
	bool isMemberToDeleteFound = true;

	//1. Declare a name for the guild(user input)
	cout << "Please enter a name for the GUILD: ";
	cin >> guildName;

	//2. Declare an initial size for the guild(user input)
	cout << "How many members are there? ";
	getValidInput(guildSize);

	string* guildMembers = new string[guildSize]; // Dynamic array of strings

	//3. Input the usernames of the initial members(user input)
	cout << "Please enter the username of the members." << endl;
	for (int i = 0; i < guildSize; i++)
	{
		cout << "Member" << i + 1 << ": ";
		cin >> userName;
		guildMembers[i] = userName;
	}

	cout << "\nInitialization complete, welcome to " << guildName << ", GUILD leader.\n" << endl;
	system("pause");
	system("cls");

	//4. Operations:
	while (isProgramRunning)
	{
		cout << "Guild Name: " << guildName << endl;
		cout << "Available Operations:" << endl;
		cout << "[1] Show Members    [2] Rename Member   [3] Add Member    [4] Delete Member    [5] Exit" << endl;
		cout << "Please enter a choice: ";
		getValidInput(operationChoice);

		switch (operationChoice)
		{
		case 1:
			printMembers(guildName, guildSize, guildMembers);
			break;

		case 2:
		// Renaming a member
			// Disply members for convenience
			printMembers(guildName, guildSize, guildMembers);
			cout << endl;

			// Ask user to input the name of the member to rename
			cout << "Please enter the username of the member to rename: ";
			cin >> memberToRename;

			// Loop through the array until the name of the member is found
			for (int k = 0; k < guildSize; k++)
			{
				// If on the last iteration and userName is not found, output error message
				if ((k == (guildSize - 1)) && (guildMembers[k] != memberToRename))
				{
					cout << "Username not found!" << endl;
					isMemberToRenameFound = false;
					break;
				}

				// If found, get index of that member
				if (guildMembers[k] == memberToRename)
				{
					indexGuildMemberRename = k;
					isMemberToRenameFound = true;
					break;
				}
			}

			// Rename if user is found
			if (isMemberToRenameFound == true)
			{
				cout << "Renaming " << guildMembers[indexGuildMemberRename] << endl;
				cout << "What is the new username of " << guildMembers[indexGuildMemberRename] << "? ";
				cin >> newUsername;
				cout << "\nSuccess. " << guildMembers[indexGuildMemberRename] << " is now " << newUsername;
				guildMembers[indexGuildMemberRename] = newUsername;
				cout << endl;
			}
			break;

		case 3:
		//  Adding a member
			// Ask for userName of new member, update the arraySize
			cout << "Enter the username of the new member: ";
			cin >> userName;
			
			// Transfer the contents of the array to a temporary one
			tempArray = new string[guildSize];
			for (int i = 0; i < guildSize; i++)
			{
				tempArray[i] = guildMembers[i];
			}

			// Delete old array
			delete[] guildMembers;

			// Increment guildSize
			guildSize++;

			// Make new array
			guildMembers = new string[guildSize];

			// Transfer contents from temp to new array
			for (int j = 0; j < guildSize - 1; j++)
			{
				guildMembers[j] = tempArray[j];
			}

			// Delete the temp array
			delete[] tempArray;

			// Add the new member in the last index of the array
			guildMembers[guildSize - 1] = userName; 

			cout << "\n" << userName << " has successfully been added to " << guildName << "!" << endl;
			break;

		case 4: 
			//  Deleting a member[EMPTY SLOT]
				// Display members for convenience
			printMembers(guildName, guildSize, guildMembers);
			cout << endl;
			// Ask for the username of the member to be deleted
			cout << "Please enter the username of the member to be deleted: " << endl;
			cout << "Option: ";
			cin >> deleteUsername;

			// Loop through the array until userName is found
			for (int l = 0; l < guildSize; l++)
			{
				if ((l == guildSize - 1) && (guildMembers[l] != deleteUsername))
				{
					cout << "Username not found!" << endl;
					isMemberToDeleteFound = false;
					break;
				}

				if (guildMembers[l] == deleteUsername)
				{
					indexGuildMemberDelete = l;
					isMemberToDeleteFound = true;
					break;
				}
			}

			// If member to delete is found, delete contents of that index of the array
			if (isMemberToDeleteFound == true)
			{
				// By moving all the members after that index backwards by 1
				// The contents of the index of the member to be deleted will be replaced by the contents of the next index
				for (int m = indexGuildMemberDelete; m < (guildSize - indexGuildMemberDelete - 1); m++)
				{
					guildMembers[m] = guildMembers[m + 1]; //acces violation here
				}

				// Make the last element empty (NULL doesn't work?)
				guildMembers[guildSize - 1] = "";

				// Then update the arraySize
				// Transfer the contents of the array to a temporary one
				tempArray = new string[guildSize];
				for (int i = 0; i < guildSize; i++)
				{
					tempArray[i] = guildMembers[i];
				}

				// Delete old array
				delete[] guildMembers;

				// Decrement guildSize.
				guildSize--;

				// Make new array
				guildMembers = new string[guildSize];

				// Transfer contents from temp to new array
				for (int j = 0; j < guildSize; j++)
				{
					guildMembers[j] = tempArray[j];
				}

				// Delete the temp array
				delete[] tempArray;

				// Success message
				cout << "Sucessfully deleted!" << endl;
			}
			break;

		case 5:
			// Exit
			isProgramRunning = false;
			cout << "Program ended." << endl;
			break;

		default:
			cout << "Invalid Choice." << endl;
			cin.clear();
			cin.ignore();
		}

		cout << endl;
		system("pause");
		system("cls");

		// When a member is added or deleted, you are required to re - size the guild.
		// The program must be in a loop
	}

	// Freeing memory
	delete[] guildMembers;
	cout << "Memory freed!" << endl;

	return 0;
}
