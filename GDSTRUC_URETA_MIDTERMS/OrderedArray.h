#pragma once
#include <assert.h>
#include <iostream>

using namespace std;

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		//your code goes after this line	
		int pointer;

		// pointer is the current index + 1, start from the back, we will start with a blank index at the back of the array . If # of elements is 0 make it also 0
		pointer = ((mNumElements > 0) ?  mNumElements : 0);  

		// While pointer is not at the first index && the value to be inserted is less than the value of the the element before the pointer
		while ((pointer > 0) && (value < mArray[pointer - 1]))
		{
			// This also shifts elements one step to the right as the pointer moves backwards through the array
			mArray[pointer] = mArray[pointer - 1];
			pointer--;
		}

		// Once loop conditions are broken, it must mean that pointer is the index where we insert the value 
		mArray[pointer] = value;
		mNumElements++;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val)
	{
		//your code goes after this line
		// Iterative binarySearch
		int counter = 0;
		int left = 0;
		int right = mNumElements - 1;
		
		while (left <= right)
		{
			counter++;
			int mid = left + ((right - left) / 2);
			if (mArray[mid] == val)
			{
				cout << "Binary Search took " << counter << " comparisons." << endl;
				return mid;
			}
			else if (val < mArray[mid]) // Meaning val is on the left side 
				right = mid - 1;
			else  // val is on the right side 
				left = mid + 1;
		}
			return -1; // return false once left <= right, meaning the value isn't in the array 
	}

private:
	T* mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};