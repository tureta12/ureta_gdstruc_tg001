#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	int input = 0;
	int size;
	int result;
	bool isRunning = true;

	cout << "Enter size of array: ";
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "Generated array!" << endl;
	system("pause");
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << " ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << " "; 

	while (isRunning)
	{
		int input;

		system("cls");
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << " ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << " "; 

		cout << "\n\nWhat would you like to do?" << endl;
		cout << "1 - Remove element at index" << endl;
		cout << "2 - Search for element" << endl;
		cout << "3 - Expand and generate random values" << endl;
		cout << "4 - Exit" << endl;

		cout << "\nChoice: ";
		cin >> input;
		cout << endl;

		switch (input)
		{
		case 1:
			// REMOVE
			cout << "Enter the index to remove: ";
			cin >> input;
			cout << "Removing elements " << unordered[input] << " (Unordered) and " << ordered[input] << " (Ordered)!" << endl;
			unordered.remove(input);
			ordered.remove(input);
			cout << "Elements removed!" << endl;

			// Print out for reference
				cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " "; 

			cout << endl;
			system("pause");
			break;

		case 2:
			// SEARCH
			cout << "Please enter a number to search: ";
			cin >> input;
			cout << endl;

			result = unordered.linearSearch(input);
			if (result >= 0)
			{
				cout << "Unordered Array(Linear Search):\n";
				cout << input << " was found at index " << result << ".\n";
			}
			else
				cout << input << " not found." << endl;

			cout << endl;

			result = ordered.binarySearch(input);
			if (result >= 0)
			{
				cout << "Ordered Array(Binary Search):\n";
				cout << input << " was found at index " << result << ".\n";
				// I didn't put a not found message in the ordered array since if it's not in the unordered then it's also not in the ordered.
			}
			system("pause");
			break;

		case 3:
			// EXPAND AND GENERATE RANDOM VALUES
			cout << "Input size of expansion: ";
			cin >> input;

			cout << "Arrays have been expanded: " << endl;
			for (int i = 0; i < input; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			// Print out for reference
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";

			cout << endl;
			system("pause");
			break;

		case 4:
			cout << "Program exited!" << endl;
			system("pause");
			isRunning = false;
			break;

		default:
			cout << "Invalid Input!" << endl;
			cin.clear();
			cin.ignore();
		}
	}
}